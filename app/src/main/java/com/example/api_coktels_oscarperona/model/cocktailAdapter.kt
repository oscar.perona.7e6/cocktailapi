package layout

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.api_coktels_oscarperona.R
import com.example.api_coktels_oscarperona.databinding.ItemCoktailBinding
import com.example.api_coktels_oscarperona.model.Drink
import com.example.api_coktels_oscarperona.viewmodel.OnClickListener

class CocktailAdapter(private val cocktails: List<Drink>, private val listener: OnClickListener):
    RecyclerView.Adapter<CocktailAdapter.ViewHolder>() {
    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemCoktailBinding.bind(view)
        fun setListener(coktail: Drink){
            binding.root.setOnClickListener {
                listener.onClick(coktail)
            }
        }
    }


    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_coktail, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return cocktails.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cocktail = cocktails[position]
        with(holder){
            setListener(cocktail)
            binding.coctelName.text = cocktail.strDrink
            binding.coctelId.text = cocktail.idDrink.toString()
            Glide.with(context)
                .load(cocktail.strDrinkThumb)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .circleCrop()
                .into(binding.image)
        }
    }

}

