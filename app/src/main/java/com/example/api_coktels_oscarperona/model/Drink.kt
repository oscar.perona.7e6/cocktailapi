package com.example.api_coktels_oscarperona.model

data class Drink(
    val idDrink: String,
    val strDrink: String,
    val strDrinkThumb: String
)