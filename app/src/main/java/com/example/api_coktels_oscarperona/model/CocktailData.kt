package com.example.api_coktels_oscarperona.model

data class CocktailData(
    var drinks: List<Drink>
)