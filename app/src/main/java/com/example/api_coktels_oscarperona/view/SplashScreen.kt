package com.example.api_coktels_oscarperona.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import com.example.api_coktels_oscarperona.R

class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        val splashScreen = installSplashScreen()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        splashScreen.setKeepOnScreenCondition{ true }
        //Lògica que vulguem executar abans d'obrir el següent activity
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

}