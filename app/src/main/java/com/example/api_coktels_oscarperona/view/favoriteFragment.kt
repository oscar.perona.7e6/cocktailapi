package com.example.api_coktels_oscarperona.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.api_coktels_oscarperona.databinding.FragmentFavoriteBinding
import com.example.api_coktels_oscarperona.model.Drink
import com.example.api_coktels_oscarperona.viewmodel.CocktailViewModel
import layout.FavoriteAdapter

private lateinit var favoriteAdapter: FavoriteAdapter
private lateinit var linearLayoutManager: RecyclerView.LayoutManager
private lateinit var binding: FragmentFavoriteBinding

class favoriteFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFavoriteBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun getFavorites(): MutableList<Drink>{
        val cocktails = mutableListOf<Drink>()
        cocktails.add(Drink("1", "Nom","url_imatge"))
        return cocktails
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        favoriteAdapter = FavoriteAdapter(getFavorites(), this)
        linearLayoutManager = LinearLayoutManager(context)

        val cocktailViewModel = ViewModelProvider(requireActivity())[CocktailViewModel::class.java]

        cocktailViewModel.fetchFavoriteDate()

        cocktailViewModel.data.observe(viewLifecycleOwner) {
            setUpRecyclerView(it!!.drinks)
        }
    }

    private fun setUpRecyclerView(cocktails: List<Drink>) {
        favoriteAdapter = FavoriteAdapter(cocktails, this)
        linearLayoutManager = LinearLayoutManager(context)
        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = favoriteAdapter
        }
    }

    fun onClick(cocktail: Drink) {
        val action = favoriteFragmentDirections.actionFavouritesFragmentToDetailFragment(cocktail.idDrink.toInt(), "favorite")
        findNavController().navigate(action)
    }
}