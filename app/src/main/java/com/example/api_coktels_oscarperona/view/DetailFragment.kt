package com.example.api_coktels_oscarperona.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.api_coktels_oscarperona.PatronSingleton.CocktailAplication
import com.example.api_coktels_oscarperona.R
import com.example.api_coktels_oscarperona.databinding.FragmentDetailBinding
import com.example.api_coktels_oscarperona.model.CocktailData
import com.example.api_coktels_oscarperona.model.Drink
import com.example.api_coktels_oscarperona.roomdatabase.ContactEntity
import com.example.api_coktels_oscarperona.viewmodel.CocktailViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailFragment : Fragment() {

    private lateinit var binding: FragmentDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentDetailBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewModel = ViewModelProvider(requireActivity())[CocktailViewModel::class.java]
        val id = arguments?.getInt("id")
        val lastFragment = arguments?.getString("lastFragment")
        val cocktail = viewModel.cocktailData(id!!)


        var favoriteStatus = 0
        CoroutineScope(Dispatchers.IO).launch {
            val algo = CocktailAplication.database.contactDao().getContactsByName(cocktail!!.strDrink).isNotEmpty()

            if (algo) {
                binding.icon.setBackgroundResource(R.drawable.ic_baseline_star_24)
                favoriteStatus = 1
            }
            else {
                binding.icon.setBackgroundResource(R.drawable.ic_baseline_star_border_24)
            }
        }

        Glide.with(requireContext())
            .load(cocktail!!.strDrinkThumb)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(binding.imageView)
        binding.cocktailName.text = cocktail!!.strDrink


        binding.icon.setOnClickListener{
            if (favoriteStatus == 0) {
                favoriteStatus++
                binding.icon.setBackgroundResource(R.drawable.ic_baseline_star_24)
                CoroutineScope(Dispatchers.IO).launch {
                    val newCocktail = ContactEntity( cocktail!!.idDrink.toLong(), cocktail!!.idDrink, cocktail!!.strDrink, cocktail!!.strDrinkThumb)
                    CocktailAplication.database.contactDao().addContact(newCocktail)
                }
            }
            else {
                favoriteStatus--
                binding.icon.setBackgroundResource(R.drawable.ic_baseline_star_border_24)
                CoroutineScope(Dispatchers.IO).launch {
                    val newCocktail = ContactEntity( cocktail!!.idDrink.toLong(), cocktail!!.idDrink, cocktail!!.strDrink, cocktail!!.strDrinkThumb)
                    CocktailAplication.database.contactDao().deleteContact(newCocktail)
                }
            }
        }
        binding.backButto.setOnClickListener{
            if (lastFragment == "recyclerView"){
                findNavController().navigate(DetailFragmentDirections.actionDetailFragmentToRecyclerViewFragment())
            }
            else if(lastFragment == "Search") {
                findNavController().navigate(DetailFragmentDirections.actionDetailFragmentToSearchFragment2())
            }
            else{
                findNavController().navigate(DetailFragmentDirections.actionDetailFragmentToFavouritesFragment())
            }
        }

    }
}