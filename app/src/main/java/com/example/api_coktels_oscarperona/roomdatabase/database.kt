package com.example.api_coktels_oscarperona.roomdatabase

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "ContactEntity")
data class ContactEntity(
    @PrimaryKey(autoGenerate = true) var id: Long = 0,
    val idDrink: String,
    val strDrink: String,
    val strDrinkThumb: String)