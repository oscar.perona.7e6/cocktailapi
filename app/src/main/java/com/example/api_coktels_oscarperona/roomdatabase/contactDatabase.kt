package com.example.api_coktels_oscarperona.roomdatabase

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.api_coktels_oscarperona.dao.ContactDao

@Database(entities = arrayOf(ContactEntity::class), version = 1)
abstract class ContactDatabase: RoomDatabase() {
    abstract fun contactDao(): ContactDao
}
