package com.example.api_coktels_oscarperona.PatronSingleton

import android.app.Application
import androidx.room.Room
import com.example.api_coktels_oscarperona.roomdatabase.ContactDatabase

class CocktailAplication: Application() {
    companion object {
        lateinit var database: ContactDatabase
    }
    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(this,
            ContactDatabase::class.java,
            "ContactDatabase").build()
    }
}




