package com.example.api_coktels_oscarperona.dao

import androidx.room.Dao
import androidx.room.*
import com.example.api_coktels_oscarperona.roomdatabase.ContactEntity

@Dao
interface ContactDao {
    @Query("SELECT * FROM ContactEntity")
    fun getAllContacts(): MutableList<ContactEntity>
    @Query("SELECT * FROM ContactEntity where strDrink = :contactName")
    fun getContactsByName(contactName: String): MutableList<ContactEntity>
    @Insert
    fun addContact(contactEntity: ContactEntity)
    @Update
    fun updateContact(contactEntity: ContactEntity)
    @Delete
    fun deleteContact(contactEntity: ContactEntity)
}
