package com.example.api_coktels_oscarperona.viewmodel


class Repository {
    val apiInterface = ApiInterface.create()

    suspend fun getCocktails() = apiInterface.getCocktails()
    //suspend fun getCocktailDetall(drinkId:Int) = apiInterface.getCocktailDetall(drinkId)

}