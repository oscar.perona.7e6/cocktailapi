package com.example.api_coktels_oscarperona.viewmodel

import com.example.api_coktels_oscarperona.model.Drink

interface OnClickListener {
    fun onClick(cocktail: Drink)
}
