package com.example.api_coktels_oscarperona.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.api_coktels_oscarperona.PatronSingleton.CocktailAplication
import com.example.api_coktels_oscarperona.model.CocktailData
import com.example.api_coktels_oscarperona.model.Drink
import com.example.api_coktels_oscarperona.roomdatabase.ContactDatabase
import com.example.api_coktels_oscarperona.roomdatabase.ContactEntity
//import com.example.api_coktels_oscarperona.model.DrinkDetall
//import com.example.api_coktels_oscarperona.model.detallCocktail
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CocktailViewModel: ViewModel() {

    private val repository = Repository()
    var data = MutableLiveData<CocktailData>().apply { listOf<Drink>() }
    var name = ""

    fun fetchData(){
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getCocktails()
            withContext(Dispatchers.Main) {
                if(response.isSuccessful){
                    data.postValue(response.body())
                }
            }
        }
    }

    fun fetchFavoriteDate(){
        CoroutineScope(Dispatchers.IO).launch {
            val listDrink = CocktailAplication.database.contactDao().getAllContacts().map { ContactEntity -> Drink(ContactEntity.idDrink, ContactEntity.strDrink, ContactEntity.strDrinkThumb) }
            data.postValue(CocktailData(listDrink))
        }
    }

    fun cocktailData(id:Int):Drink?{
        var drink:Drink? = null
        var i = 0

        while (i in data.value!!.drinks.indices&&drink==null){
            if(data.value!!.drinks[i].idDrink == id.toString()) drink = data.value!!.drinks[i]
            i++
        }

        return drink
    }

    fun searchCocktail(name:String):List<Drink>{
        val characters = mutableListOf<Drink>()
        for(i in data.value?.drinks!!.indices){
            if(name in data.value?.drinks!![i].strDrink) characters.add(data.value?.drinks!![i])
        }
        return characters
    }
}