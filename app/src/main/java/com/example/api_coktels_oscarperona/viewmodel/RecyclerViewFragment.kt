package com.example.api_coktels_oscarperona.viewmodel

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.api_coktels_oscarperona.databinding.FragmentRecyclerViewBinding
import com.example.api_coktels_oscarperona.model.Drink
import layout.CocktailAdapter

class RecyclerViewFragment : Fragment(), OnClickListener {

    private lateinit var cocktailAdapter: CocktailAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentRecyclerViewBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRecyclerViewBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun getCocktails(): MutableList<Drink>{
        val cocktails = mutableListOf<Drink>()
        cocktails.add(Drink("1", "Nom","url_imatge"))
        return cocktails
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cocktailAdapter = CocktailAdapter(getCocktails(), this)
        linearLayoutManager = LinearLayoutManager(context)

        val cocktailViewModel = ViewModelProvider(requireActivity())[CocktailViewModel::class.java]

        cocktailViewModel.fetchData()

        cocktailViewModel.data.observe(viewLifecycleOwner) {
            setUpRecyclerView(it!!.drinks)
        }
    }

    private fun setUpRecyclerView(cocktails: List<Drink>) {
        cocktailAdapter = CocktailAdapter(cocktails, this)
        linearLayoutManager = LinearLayoutManager(context)
        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = cocktailAdapter
        }
    }

    override fun onClick(cocktail: Drink) {
        val action = RecyclerViewFragmentDirections.actionRecyclerViewFragmentToDetailFragment(
                cocktail.idDrink.toInt(),
                "recyclerView"
            )
        findNavController().navigate(action)
    }

}