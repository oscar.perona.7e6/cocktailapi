package com.example.api_coktels_oscarperona.viewmodel

import com.example.api_coktels_oscarperona.model.CocktailData
//import com.example.api_coktels_oscarperona.model.detallCocktail
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path


interface ApiInterface {

    @GET("filter.php?a=Alcoholic")
    suspend fun getCocktails(): Response<CocktailData>



//    @GET("lookup.php?i={idDrink}")
//    suspend fun getCocktailDetall(@Path("idDrink") drinkId: Int): Response<detallCocktail>

        companion object {
            val BASE_URL = "https://www.thecocktaildb.com/api/json/v1/1/"
            fun create(): ApiInterface {
                val client = OkHttpClient.Builder().build()
                val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build()
                return retrofit.create(ApiInterface::class.java)
            }
        }

    /*
    www.thecocktaildb.com/api/json/v1/1/
    www.thecocktaildb.com/api/json/v1/1/filter.php?a=Alcoholic ----> TODOS LOS COCKTELES
    www.thecocktaildb.com/api/json/v1/1/lookup.php?i=11007  -------> COCKTELES POR ID
     */

    }