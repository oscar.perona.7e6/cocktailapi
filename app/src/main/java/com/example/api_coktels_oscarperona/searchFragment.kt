package com.example.api_coktels_oscarperona

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.api_coktels_oscarperona.databinding.FragmentSearchBinding
import com.example.api_coktels_oscarperona.model.Drink
import com.example.api_coktels_oscarperona.viewmodel.CocktailViewModel
import com.example.api_coktels_oscarperona.viewmodel.OnClickListener
import layout.CocktailAdapter


class SearchFragment : Fragment(), OnClickListener {

    private lateinit var binding: FragmentSearchBinding
    private lateinit var cocktailAdapter: CocktailAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager

    private fun getCharacters(): MutableList<Drink>{
        val cocktails = mutableListOf<Drink>()
        return cocktails
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentSearchBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cocktailAdapter= CocktailAdapter(getCharacters(),this)
        linearLayoutManager = LinearLayoutManager(context)

        val viewModel = ViewModelProvider(requireActivity())[CocktailViewModel::class.java]

        binding.searchImg.setOnClickListener{
            val name = binding.imputSearch.text.toString()
            binding.imputSearch.setText("")
            setUpRecycler(viewModel.searchCocktail(name))
        }
    }

    override fun onClick(cocktail: Drink) {
        val action = SearchFragmentDirections.actionSearchFragmentToDetailFragment3(cocktail.idDrink.toInt(), "Search")
        findNavController().navigate(action)
    }

    fun setUpRecycler(resultList:List<Drink>){
        val myAdapter = CocktailAdapter(resultList,this)
        binding.recyclerView.apply {
            setHasFixedSize(true)
            adapter = myAdapter
            layoutManager = linearLayoutManager
        }
    }
}